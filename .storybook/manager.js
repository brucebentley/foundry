// import { create } from '@storybook/theming/create';
import { addons } from '@storybook/addons';
import theme from './theme'

addons.setConfig({
  isFullscreen: false,
  showAddonsPanel: true,
  panelPosition: 'right',
  theme,
  // theme: create({
  //   base: 'light',
  //   brandTitle: 'GoCanvas: Foundry Design System',
  //   brandUrl: 'https://gitlab.com/brucebentley/foundry/',
  //   brandImage: 'https://cdn.gocanvas.com/images/canvas_logo.svg',
  //   gridCellSize: 12,
  // }),
});
