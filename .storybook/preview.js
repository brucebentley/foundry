import { addParameters, addDecorator } from '@storybook/react';
import React from 'react';
import Wrapper from '../foundry/utils/wrapper';
import '~styles/foundry.scss';
import '~styles/storybook.scss';

addDecorator(storyFn => <Wrapper>{storyFn()}</Wrapper>);
addParameters({
  options: {
    storySort: (a, b) =>
      a[1].kind === b[1].kind ? 0 : a[1].id.localeCompare(b[1].id, undefined, { numeric: true }),
  },
});
