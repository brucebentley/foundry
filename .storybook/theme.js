import { create } from '@storybook/theming'

export default create({
  base: 'dark',

  // UI
  appBg: '#203147',
  appContentBg: '#ffffff',

  // Toolbar Default And Active Colors
  barTextColor: '#414042',
  barBg: '#f3f3f4',

  // Typography
  fontBase: '"proxima-nova", sans-serif',

  // Text Colors
  textColor: '#ffffff',
  textInverseColor: '#414042',

  // Form Colors
  inputBg: '#ffffff',
  inputBorder: '#c0c0c0',
  inputTextColor: '#ffffff',

  // Brand Assets
  brandTitle: `GoCanvas: Foundry Design System`,
  brandUrl: 'https://gocanvas.com/',
  brandImage: 'https://cdn.gocanvas.com/images/canvas_logo.svg',
})
