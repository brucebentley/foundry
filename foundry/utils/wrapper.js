import React from 'react';

const styles = {
  width: '100%',
  maxWidth: '714px',
};

export default ({ children }) => <div style={styles}>{children}</div>;
