import React from 'react';
import { meta } from '~tokens/meta';
import Wrapper from './wrapper';
import canvasLogo from '~assets/logo-drop-blue.svg';

export const StoryHeader = ({ title, children }) => (
  <header className="sb--header">
    <div className="sb--meta">
      <div className="sb--meta__label">
        <img className="sb--meta__icon" src={canvasLogo} />
        Foundry Design System
      </div>
      <span className="sb--meta__version">Last major update: {meta.lastUpdated}</span>
    </div>
    <Wrapper>
      <div className="sb--header_wrap">
        <h1 className="sb--header__title">{title}</h1>
        <div className="sb--header__subHead">{children}</div>
      </div>
    </Wrapper>
  </header>
);

export const StorySection  = ({ children }) => (
  <section>
    {children}
  </section>
);

export const StoryArticle = ({ children }) => (
  <article>
    <div className="article--content">{children}</div>
  </article>
);

export const paths = {
  '0-overview': 'Overview',
  '01-foundation': 'Foundation',
  '02-components': 'Components',
}

export const determineRoot = base => {
  const roots = Object.keys(paths)
  const root = roots.find(root => base.includes(root))
  return paths[root] || 'To Do'
}
